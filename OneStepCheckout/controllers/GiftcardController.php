<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_One_Step_Checkout
 * @copyright   Copyright (c) 2015 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */


require_once('Plumrocket/OneStepCheckout/controllers/CheckoutController.php');
class Plumrocket_Onestepcheckout_GiftcardController extends Plumrocket_Onestepcheckout_CheckoutController
{

    /**
     * No index action, forward to 404
     *
     */
    public function indexAction()
    {
        $this->_forward('noRoute');
    }


    /**
     * Add Gift Card to current quote
     *
     */
    public function addAction()
    {
        $message = null;
        $data = $this->getRequest()->getPost();
        if (isset($data['giftcard_code'])) {
            $code = $data['giftcard_code'];
            try {
                if (strlen($code) > Enterprise_GiftCardAccount_Helper_Data::GIFT_CARD_CODE_MAX_LENGTH) {
                    $message = Mage::helper('enterprise_giftcardaccount')->__('Wrong gift card code.');
                }
                Mage::getModel('enterprise_giftcardaccount/giftcardaccount')
                    ->loadByCode($code)
                    ->addToCart();
                $this->getOnepage()->getQuote()->collectTotals()->save();
                $message = array(
                    'update_section' => array(
                        'payment-method' => array('name' => 'payment-method', 'html' => $this->_getPaymentMethodsHtml()),
                        'review' => array('name' => 'review', 'html' => $this->_getReviewHtml())
                    )
                );
            } catch (Mage_Core_Exception $e) {
                Mage::dispatchEvent('enterprise_giftcardaccount_add', array('status' => 'fail', 'code' => $code));
                $message = $e->getMessage();
            } catch (Exception $e) {
                $message = $this->__('Cannot apply gift card.');
            }
        }
        return $this->sendResponse($message);
    }


    public function removeAction()
    {
        $message = null;
        if ($code = $this->getRequest()->getParam('code')) {
            try {
                Mage::getModel('enterprise_giftcardaccount/giftcardaccount')
                    ->loadByCode($code)
                    ->removeFromCart();
                $this->getOnepage()->getQuote()->collectTotals()->save();
                $message = array(
                    'update_section' => array(
                        'payment-method' => array('name' => 'payment-method', 'html' => $this->_getPaymentMethodsHtml()),
                        'review' => array('name' => 'review', 'html' => $this->_getReviewHtml())
                    )
                );
            } catch (Mage_Core_Exception $e) {
                $message = $e->getMessage();
            } catch (Exception $e) {
                $message = $this->__('Cannot remove gift card.');
            }
        }
        return $this->sendResponse($message);
    }


    public function removerewardsAction()
    {
        $message = null;
        if (!Mage::helper('enterprise_reward')->isEnabledOnFront()
            || !Mage::helper('enterprise_reward')->getHasRates()) {
            return $this->_redirect('customer/account/');
        }

        $quote = Mage::getSingleton('checkout/session')->getQuote();

        if ($quote->getUseRewardPoints()) {
            $quote->setUseRewardPoints(false)->collectTotals()->save();
            $message = array(
                'update_section' => array(
                    'payment-method' => array('name' => 'payment-method', 'html' => $this->_getPaymentMethodsHtml()),
                    'review' => array('name' => 'review', 'html' => $this->_getReviewHtml())
                )
            );
        } else {
            $message = $this->__('Reward points will not be used in this order.');
        }

        return $this->sendResponse($message);
    }


    public function removestorecreditAction()
    {
        $message = null;
        if (!Mage::helper('enterprise_customerbalance')->isEnabled()) {
            $this->_redirect('customer/account/');
            return;
        }

        $quote = Mage::getSingleton('checkout/session')->getQuote();
        if ($quote->getUseCustomerBalance()) {
            $quote->setUseCustomerBalance(false)->collectTotals()->save();
            $message = array(
                'update_section' => array(
                    'payment-method' => array('name' => 'payment-method', 'html' => $this->_getPaymentMethodsHtml()),
                    'review' => array('name' => 'review', 'html' => $this->_getReviewHtml())
                )
            );
        } else {
            $message = $this->__('Store Credit payment is not being used in your shopping cart.');
        }

        return $this->sendResponse($message);
    }


    private function sendResponse($message = '')
    {
        $result = null;
        if ( $message && is_array($message) ) {
            $result = $message;
        } else if($message){
            $result = array(
                'error' => 1,
                'message' => $this->__($message)
            );
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

}
