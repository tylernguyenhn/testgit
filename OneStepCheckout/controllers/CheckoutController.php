<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_One_Step_Checkout
 * @copyright   Copyright (c) 2015 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */


require_once('Mage/Checkout/controllers/OnepageController.php');
class Plumrocket_Onestepcheckout_CheckoutController extends Mage_Checkout_OnepageController
{

    protected $_isSaveAll = false;


    /**
     * Get shipping method step html
     *
     * @return string
     */
    protected function _getShippingMethodsHtml()
    {
        if ($this->_isSaveAll) { return; }
        $this->loadLayout('onestepcheckout_onepage_updateblocks');
        return $this->getLayout()->getBlock('shipping.root')->toHtml();
    }

    /**
     * Get payment method step html
     *
     * @return string
     */
    protected function _getPaymentMethodsHtml()
    {
        if ($this->_isSaveAll) { return; }
        $this->loadLayout('onestepcheckout_onepage_updateblocks');
        return $this->getLayout()->getBlock('payment.root')->toHtml();
    }

    /**
     * Get review html
     *
     * @return string
     */
    protected function _getReviewHtml()
    {
        if ($this->_isSaveAll) { return; }
        $this->loadLayout('onestepcheckout_onepage_updateblocks');
        return $this->getLayout()->getBlock('review.root')->toHtml();
    }

    /**
     * Save checkout method
     */
    public function saveMethodAction( $sendResponse = true )
    {
        if ($this->_expireAjax() || !Mage::helper('onestepcheckout')->moduleEnabled()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $method = $this->getRequest()->getPost('method');
            $result = $this->getOnepage()->saveCheckoutMethod($method);
            
            if ( $sendResponse ){
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            } else {
                return $result;
            }
        }
    }

    /**
     * Save checkout billing address
     */
    public function saveBillingAction( $sendResponse = true )
    {
        if ($this->_expireAjax() || !Mage::helper('onestepcheckout')->moduleEnabled()) {
            return;
        }

        if (Mage::getSingleton('plumbase/observer')->customer() != Mage::getSingleton('plumbase/product')->currentCustomer()) {
            return;
        }

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('billing', array());
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }

            if ( ($this->getOnepage()->getQuote()->getCheckoutMethod() == 'register') && !isset($data['customer_password']) ) {
                $data['customer_password'] = $data['confirm_password'] = Mage::helper('core')->getRandomString($length = 7);
            }

            $result = $this->getOnepage()->saveBilling($data, $customerAddressId);

            if (!isset($result['error'])) {
                $result = array();
                $result['update_section'][0] = array('name' => 'payment-method', 'html' => $this->_getPaymentMethodsHtml());

                if (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1 && !$this->getOnepage()->getQuote()->isVirtual()) {
                    $result['update_section'][1] = array('name' => 'shipping-method', 'html' => $this->_getShippingMethodsHtml());
                }
            }

            if ( $sendResponse ){
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            } else {
                return $result;
            }
        }
    }

    /**
     * Shipping address save action
     */
    public function saveShippingAction( $sendResponse = true )
    {
        if ($this->_expireAjax() || !Mage::helper('onestepcheckout')->moduleEnabled()) {
            return;
        }

        if (Mage::getSingleton('plumbase/observer')->customer() != Mage::getSingleton('plumbase/product')->currentCustomer()) {
            return;
        }

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping', array());
            $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
            $result = $this->getOnepage()->saveShipping($data, $customerAddressId);

            if (!isset($result['error'])) {
                $result['update_section'] = array(
                    'name' => 'shipping-method',
                    'html' => $this->_getShippingMethodsHtml()
                );
            }

            if ( $sendResponse ){
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            } else {
                return $result;
            }
        }
    }

    /**
     * Shipping method save action
     */
    public function saveShippingMethodAction( $sendResponse = true )
    {
        if ($this->_expireAjax() || !Mage::helper('onestepcheckout')->moduleEnabled()) {
            return;
        }

        if (Mage::getSingleton('plumbase/observer')->customer() != Mage::getSingleton('plumbase/product')->currentCustomer()) {
            return;
        }

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping_method', '');
            $result = $this->getOnepage()->saveShippingMethod($data);

            // $result will contain error data if shipping method is empty
            if (empty($result)) {

                Mage::dispatchEvent(
                    'checkout_controller_onepage_save_shipping_method',
                    array(
                        'request' => $this->getRequest(), 
                        'quote'   => $this->getOnepage()->getQuote())
                );

                $this->getOnepage()->getQuote()->collectTotals()->save();

                $result['update_section'][0] = array(
                    'name' => 'payment-method',
                    'html' => $this->_getPaymentMethodsHtml()
                );
                $result['update_section'][1] = array(
                    'name' => 'review',
                    'html' => $this->_getReviewHtml()
                );
            }

            if ( $sendResponse ){
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            } else {
                return $result;
            }
        }
    }

    public function saveGiftWrappingAction()
    {
        Mage::dispatchEvent(
            'checkout_controller_onepage_save_shipping_method',
            array(
                'request' => $this->getRequest(), 
                'quote'   => $this->getOnepage()->getQuote())
        );

        $this->getOnepage()->getQuote()->collectTotals()->save();

        $result['update_section'][1] = array(
            'name' => 'review',
            'html' => $this->_getReviewHtml()
        );

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
    

    /**
     * Save payment ajax action
     *
     * Sets either redirect or a JSON response
     */
    public function savePaymentAction( $sendResponse = true )
    {
        if ($this->_expireAjax() || !Mage::helper('onestepcheckout')->moduleEnabled()) {
            return;
        }

        if (Mage::getSingleton('plumbase/observer')->customer() != Mage::getSingleton('plumbase/product')->currentCustomer()) {
            return;
        }

        try {
            if (!$this->getRequest()->isPost()) {
                $this->_ajaxRedirectResponse();
                return;
            }

            $data = $this->getRequest()->getPost('payment', array());

            if (!isset($data['use_reward_points']) && $this->getOnepage()->getQuote()->getUseRewardPoints()) {
                $this->getOnepage()->getQuote()->setUseRewardPoints(false)->collectTotals()->save();
            }
            if ($this->getOnepage()->getQuote()->getUseCustomerBalance() && (!isset($data['use_customer_balance']) || $data['use_customer_balance'] == 0)) {
                $this->getOnepage()->getQuote()->setUseCustomerBalance(false)->collectTotals()->save();
            }
            //if (!isset($data['method']) || $data['method'] != 'free') { // do not uncomment Zero order will not work
                $result = $this->getOnepage()->savePayment($data);
            //}

            // get section and redirect data
            $redirectUrl = $this->getOnepage()->getQuote()->getPayment()->getCheckoutRedirectUrl();
            //if ( empty($result['error']) && !$redirectUrl ){
            if ( (empty($result['error']) && !$redirectUrl) || !$sendResponse ) {
                $result = array();
                $result['update_section'][0] = array(
                    'name' => 'payment-method',
                    'html' => $this->_getPaymentMethodsHtml()
                );
                $result['update_section'][1] = array(
                    'name' => 'review',
                    'html' => $this->_getReviewHtml()
                );
            }
            if ($redirectUrl) {
                $result['redirect'] = $redirectUrl;
            }
        } catch (Mage_Payment_Exception $e) {
            if ($e->getFields()) {
                $result['fields'] = $e->getFields();
            }
            $result['error'] = $e->getMessage();
        } catch (Mage_Core_Exception $e) {
            $result['error'] = $e->getMessage();
        } catch (Exception $e) {
            Mage::logException($e);
            $result['error'] = $this->__('Unable to set Payment Method.');
        }

        if ( $sendResponse ){
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } else {
            return $result;
        }
    }
    

    public function preSaveAction($sendResponse = true)
    {
        $result = array();
        if ($this->getRequest()->getParam('steps')) {
            $steps = explode(",", $this->getRequest()->getParam('steps'));
        }
        $billingData = $this->getRequest()->getParam('billing');

        $actions = array('saveBillingAction', 'saveShippingAction', 'saveShippingMethodAction', 'savePaymentAction');
        $params = array('update_section');

        if (isset($steps) && is_array($steps)) {
            foreach ( $steps as $step) {
                $actionResult = array();
                $action = $actions[$step];
                if (($action == 'saveShippingAction') && isset($billingData['use_for_shipping']) && ($billingData['use_for_shipping'] == 1)) {
                    continue;
                }
                if ($action == 'saveBillingAction'){
                    array_push($actionResult, $this->saveMethodAction(false));
                    $step_result = $this->$action(false);
                    if (isset($step_result['error'])){
                        $result = $step_result;
                        break;
                    }
                    array_push($actionResult, $step_result);

                } else {
                    $step_result = $this->$action(false);
                    if (isset($step_result['error']) && $this->_isSaveAll){
                        $result = $step_result;
                        break;
                    }
                    array_push($actionResult, $step_result);
                }

                if ( $sendResponse ){
                    foreach ($params as $param) {
                        foreach ($actionResult as $values) {
                            if(isset($values[$param])){
                                if (isset($values[$param]['name'])) {
                                    $result[$param][$values[$param]['name']] = $values[$param];
                                } else {
                                    foreach ($values[$param] as $value) {
                                        $result[$param][$value['name']] = $value;
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        if ( $sendResponse ){
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } else {
            return $result;
        }
    }


    /**
     * Create order action
     */
    public function saveAllAction()
    {
        if (!Mage::helper('onestepcheckout')->moduleEnabled()) {
            return;
        }

        $this->_isSaveAll = true;

        if (!Mage::helper('onestepcheckout')->reCaptchaVerify($this->getRequest()->getParam('g-recaptcha-response'))) {
            $result['success']  = false;
            $result['error']    = true;
            $result['validateblock']  = 'validation-g-recaptcha';
            $result['error_messages'] = $this->__('Please click on reCAPTCHA checkbox.');
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } else {
            $result = $this->preSaveAction(false);
            if (isset($result['error'])){
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            } else {
                Mage::getSingleton('checkout/session')->getMessages(true);
                if (Mage::getSingleton('plumbase/observer')->customer() == Mage::getSingleton('plumbase/product')->currentCustomer()) {
                    $payment = $this->getRequest()->getParam('payment');
                    if (isset($payment['method'])) {
                        switch($payment['method']) {
                            case 'paypaluk_express' :
                                $this->getResponse()->setBody(json_encode(array('redirect' => Mage::getUrl('paypaluk/express/start'))));
                                break;
                            case 'paypal_express' :
                                $this->getResponse()->setBody(json_encode(array('redirect' => Mage::getUrl('paypal/express/start'))));
                                break;
                            case 'paypal_express_bml' :
                                $this->getResponse()->setBody(json_encode(array('redirect' => Mage::getUrl('paypal/bml/start'))));
                                break;
                            case 'sagepayserver' :
                            case 'sagepaydirectpro' :
                            case 'sagepayform' :
                                $this->_forward('onepageSaveOrder', 'payment', 'sgps', $this->getRequest()->getParams());
                                break;
                            default:
                                $this->_isSaveAll = false;
                                $this->saveOrderAction();
                        }
                    }
                }
            }
        }
    }


    /**
     * Initialize coupon
     */
    public function couponPostAction()
    {
        if ($this->_expireAjax() || !Mage::helper('onestepcheckout')->moduleEnabled()) {
            return;
        }

        $couponCode = (string) $this->getRequest()->getParam('coupon_code');
        if ($this->getRequest()->getParam('remove') == 1) {
            $couponCode = '';
        }
        $quote = $this->getOnepage()->getQuote();
        $oldCouponCode = $quote->getCouponCode();
        if (!strlen($couponCode) && !strlen($oldCouponCode)) {
            return;
        }

        try {
            $codeLength = strlen($couponCode);
            if (defined('Mage_Checkout_Helper_Cart::COUPON_CODE_MAX_LENGTH')) {
                $isCodeLengthValid = $codeLength && $codeLength <= Mage_Checkout_Helper_Cart::COUPON_CODE_MAX_LENGTH;
            } else {
                $isCodeLengthValid = $codeLength && true;
            }

            $quote->getShippingAddress()->setCollectShippingRates(true);
            $quote->setCouponCode($isCodeLengthValid ? $couponCode : '')
                ->collectTotals()
                ->save();

            if ($codeLength) {
                if ($isCodeLengthValid && $couponCode == $quote->getCouponCode()) {
                    //$this->_getSession()->addSuccess($this->__('Coupon code "%s" was applied.', Mage::helper('core')->escapeHtml($couponCode)));
                } else {
                    $result = array(
                        'error' => 1,
                        'message' => $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->escapeHtml($couponCode))
                    );
                }
            } else {
                //$this->_getSession()->addSuccess($this->__('Coupon code was canceled.'));
            }
        } catch (Mage_Core_Exception $e) {
            $result = array(
                'error' => 1,
                'message' => $e->getMessage()
            );
        } catch (Exception $e) {
            $result = array(
                'error' => 1,
                'message' => $this->__('Cannot apply the coupon code.')
            );
        }

        $result['update_section'] = array( 
            'name' => 'review', 
            'html' => $this->_getReviewHtml()
        );
        
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    
    public function customerExistAction()
    {
        $customerCollection = Mage::getModel('customer/customer')->getCollection();

        if ( Mage::getStoreConfig('customer/account_share/scope') == "1" ) {
            $customerCollection->addFieldToFilter('website_id', Mage::app()->getStore()->getWebsiteId());
        }
        $customer = $customerCollection->addFieldToFilter('email', $this->getRequest()->getParam('email'))
            ->setPageSize(1)
            ->getFirstItem();

        $this->getResponse()->setBody(json_encode(array(
            'result' => ( $customer->getId() ) ? true : false
        )));
    }


    /**
     * Check can page show for unregistered users
     *
     * @return boolean
     */
    protected function _canShowForUnregisteredUsers()
    {
        return true;
    }

}